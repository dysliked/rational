mod rational_factory;
mod rational;

use crate::rational::Rational;

fn main() {
    let frac = Rational::new(1, 2);
    let frac1 = Rational::new(1, 4);

    println!("{} == {} ? {}", frac, frac1, frac == frac1);
}
