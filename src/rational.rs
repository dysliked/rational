use num::Integer;

use std::fmt;
use std::cmp;
use std::ops;

#[derive(Debug,Copy,Clone)]
pub struct Rational {
    num: i64,
    den: i64,
}

impl Rational {
    pub fn new(num: i64, den: i64) -> Rational {
        Rational{
            num: num,
            den: den,
        }
    }

    pub fn get_denominator(&self) -> i64 {
        self.den
    }

    pub fn get_numerator(&self) -> i64 {
        self.num
    }

    pub fn inverse(&self) -> Self {
        Rational::new(self.den, self.num)
    }

    pub fn reduce(&self) -> Self {
        let commun = self.num.gcd(&self.den);

        Rational::new(self.num / commun, self.den / commun)
    }
}

impl fmt::Display for Rational {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/{}", self.get_numerator(), self.get_denominator())
    }
}

impl cmp::PartialEq for Rational {
    fn eq(&self, other: &Self) -> bool {
        let lhs = self.reduce();
        let rhs = other.reduce();

        lhs.get_denominator() == rhs.get_denominator() && lhs.get_numerator() == rhs.get_numerator()
    }
}

impl ops::Add<Rational> for Rational {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Rational::new(self.get_numerator() * rhs.get_denominator() + rhs.get_numerator() * self.get_denominator(), self.get_denominator() * rhs.get_denominator()).reduce()
    }
}

impl ops::Div<Rational> for Rational {
    type Output = Self;

    fn div(self, rhs: Self) -> Self {
        self * rhs.inverse()
    }
}

impl ops::Mul<Rational> for Rational {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        Rational::new(self.get_numerator() * rhs.get_numerator(), self.get_denominator() * rhs.get_denominator()).reduce()
    }
}

impl ops::Neg for Rational {
    type Output = Self;

    fn neg(self) -> Self {
        Rational::new(- self.get_numerator(), self.get_denominator())
    }
}

impl ops::Sub<Rational> for Rational {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        self + (- rhs)
    }
}
