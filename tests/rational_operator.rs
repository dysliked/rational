use rational::Rational;

#[test]
fn addition() {
    let rat1 = Rational::new(1, 2);
    let rat2 = Rational::new(3, 4);
    let rat3 = Rational::new(2, 4);
    let rat4 = Rational::new(2, 3);

    assert_eq!(Rational::new(5, 4), rat1 + rat2);
    assert_eq!(Rational::new(1, 1), rat1 + rat3);
    assert_eq!(Rational::new(7, 6), rat1 + rat4);
}

#[test]
fn division() {
    let rat1 = Rational::new(1, 2);

    assert_eq!(Rational::new(1, 1), rat1 / rat1);
}

#[test]
fn subtraction() {
    let rat1 = Rational::new(1, 2);

    assert_eq!(Rational::new(-1, 2), - rat1);
    assert_eq!(Rational::new(0, 1), rat1 - rat1);
}
