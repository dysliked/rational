use crate::rational::Rational;

pub const ZERO: Rational = Rational::new(0, 1);
pub const PINF: Rational = Rational::new(1, 0);
pub const NINF: Rational = Rational::new(-1, 0);

pub fn convert_float_to_rational(nb: f64, iter: u64) -> Rational {
    if nb == 0.0 || iter == 0 {
        return ZERO;
    } else if nb < 1.0 {
        return convert_float_to_rational(1.0 / nb, iter - 1).inverse();
    }
    
    let floor = nb.floor();

    return Rational::new(floor as i64, 1) + convert_float_to_rational(nb - floor, iter - 1);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_conversion() {
        assert_eq!(Rational::new(1, 2), convert_float_to_rational(0.5, 2));
    }
}
