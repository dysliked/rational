use rational::Rational;

#[test]
fn equality() {
    let rat1 = Rational::new(1, 2);
    let rat2 = Rational::new(2, 4);
    let rat3 = Rational::new(1, 3);

    assert_eq!(rat1, rat2);
    assert_ne!(rat1, rat3);
}
